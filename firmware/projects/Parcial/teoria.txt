---------------------Teoría-------------------------

1) En la imagen podemos ver un Conversor Analógico Digital de suceciones repetitivas. 
Este funciona dividiendo el voltaje de referencia en cada bit del DAC. Luego se le asigna al MSB del DAC (correspondiente a Vref/2) un 1 y al resto de los bits
un 0. Seguido, compara la señal de entrada (cada muestra) con la tensión asignada a este bit. Si es mayor el bit queda en 1 y si es menor se pasa a 0. 
Después, independientemente del valor del bit MSB (bit n) se le asigna al bit n-1 el valor 1 y se vuelve a realizar el proceso anterior (ahora comparando con Vref/4)
y usando la misma lógica de que si es mayor el bit queda en 1 y si es menor se pasa a 0. 
Este proceso se hace para todos los bits y asi se obtiene la palabra digital para cada muestra.

	a) El tiempo de adquisición es el tiempo que le lleva al conversor quedarse con una tensión fija de la seña de entrada
	para cada muestra. Este tiempo depende de la constante RC. En cambio el tiempo de conversión es el tiempo que le lleva al conversor, convertir (realizar la lógica
	característica del conversor, codificar) el valor de la tensión de la muestra en una palabra digital.

	b) El sample and hold es un bloque dentro de los conversores que se encarga de la captura y retención del valor de una muestra. Generalmente se dibuja como un
	circuito R-C con una llave antes de la resistencia. El proceso de "sample" sería represtado por el cierre de la llave, permitiendo que la tensión de entrada (que es
	la señal a muestrear) se cierre permitiendo el paso de corriente. El "hold" lo "realiza" el capacitor al almacenar cargar y por ende mantener el voltaje que entró.

2) En la figura vemos el diagrama de bloques del funcionamiento del SysTick. El SysTick es una excepción para el microprocesador, la cual puede ser habilitada o desabilitada. 
Por ello en la dirección especificada el primer bit puede configurar esto.
El SysTick funciona con un clock de referencia y uno de procesador. Es un contador decreciente, por lo que se leasigna un valor "alto" desde el Reload Value para que vaya decreciendo. A cada decremento
envía el estado pendiente del SysTick, es decir el valor actual. Cuando el contador se decrementa de 1 a 0 se dispara 
una lógica para setear una bandera en el bit 16. 
Además el SysTick puede ser calibrado con la palabra Calibration