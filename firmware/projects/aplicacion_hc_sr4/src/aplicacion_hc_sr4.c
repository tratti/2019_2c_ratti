/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "aplicacion_hc_sr4.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "uart.h"
#include "led.h"
#include "switch.h"
#include "timer.h"
#include "hc_sr4.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>



/*==================[macros and definitions]=================================*/
#define cm 0
#define in 1
/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/



/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
uint8_t unidad=cm;
int16_t distancia;

void Tecla1()
{
	if(unidad==cm)
	{
	  	unidad=in;
	}
	else
	{
	 	unidad=cm;
	}
}

void Atencion_Interrupcion (void){
	LedToggle(LED_RGB_G);

	if (unidad==cm){
		LedOn(LED_1);
		distancia= HcSr04ReadDistanceCentimeters();//llamo la función que me devuelve la distancia en cm
		UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(distancia, 10) );
		UartSendString(SERIAL_PORT_P2_CONNECTOR, " cm \r\n");
	}
	else {
		LedOff(LED_1);
	}

	if(unidad==in){
		LedOn(LED_2);
		distancia = HcSr04ReadDistanceInches();//llamo la función que me devuelve la distancia en cm
		UartSendString(SERIAL_PORT_P2_CONNECTOR, UartItoa(distancia, 10) );
		UartSendString(SERIAL_PORT_P2_CONNECTOR, " in \r\n");
	}
	else{
		LedOff(LED_2);
	}

}

void funcion_nula (void){
}

int main(void){

	SystemClockInit();
	LedsInit();
	SwitchesInit();
	HcSr04Init(T_FIL2, T_FIL3);//llamo la funcion para que conexione ECHO con T_FIL, y TRIGGER con T_FIL3



	timer_config temp;
	temp.timer=TIMER_B;
	temp.period=1000;
	temp.pFunc=Atencion_Interrupcion;

	TimerInit(&temp);
	TimerStart(TIMER_B);

	serial_config puerto;
	puerto.port = SERIAL_PORT_P2_CONNECTOR ;
	puerto.baud_rate=9600;
	puerto.pSerial=funcion_nula;
	UartInit(&puerto);

	SwitchActivInt(SWITCH_1,Tecla1);



	while (1){
	}
	return 0;
}


/*==================[end of file]============================================*/

