/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "blinking.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "Tcrt5000.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "delay.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


bool activado=true;
bool hold=false;
uint32_t count=0;

void Tecla1()
{
	if(activado==true)
	{
	  	activado=false;
	}
	else
	{
	 	activado=true;
	}
}

void Tecla2()
{
	if(hold==false)
	{
	   hold=true;
	}
	else
	{
		hold=false;
	}
}

void Tecla3()
{
	count=0;
}


int main(void)
{
	SystemClockInit();
	LedsInit();
	bool validation = Tcrt5000Init(T_COL0); //Inicializo en el pin GPIO1 (modificable)
	SwitchesInit();


	gpio_t pins[7];
			pins[0]=LCD1;
			pins[1]=LCD2;
			pins[2]=LCD3;
			pins[3]=LCD4;
			pins[4]=GPIO1;
			pins[5]=GPIO3;
			pins[6]=GPIO5;
		ITSE0803Init(pins);


	bool p_state=1; //seteo el estado previo en alto.


	SwitchActivInt(SWITCH_1,Tecla1);
	SwitchActivInt(SWITCH_2,Tecla2);
	SwitchActivInt(SWITCH_3,Tecla3);


    while(1)
    {

    	if (activado==true)
    	{
    		LedOn(LED_RGB_R);
    		LedOn(LED_RGB_G);
    		LedOn(LED_RGB_B);

    		bool state=Tcrt5000State();

    		    	if (state==1)
    		    	{
    					LedOn(LED_1);
    					LedOn(LED_2);
    					LedOn(LED_3);

    					if(hold==false) //si esta activado el hold no cuenta pero muestra el numero
    					{
    						if(state!=p_state) //si el estado actual se distinto al estado anterior cuento
    						count++;
    					}
    					else LedOn(LED_2);

    					p_state=1; //guardo el estado
    		    	}
    		    	else
    		    	{
    		    		p_state=0; //guardo el estado
    		    		LedsOffAll();
    		    	}

    		    	ITSE0803DisplayValue(count);
    	}
    	else
    	{
    			ITSE0803DisplayOff();//paso un valor != de BCD para apagar
    			LedOn(LED_RGB_R);
    	}




	}
    

	return 0;
}

/*==================[end of file]============================================*/

