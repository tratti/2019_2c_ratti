# Encuesta con EOG

Este proyecto, implementado en una EDU-CIAA, es una encuesta la cual el usuario responde
SI o No con el movimiento de sus ojos. Mirar para arriba corresponde un SI y para abajo 
un NO.

La encuesta se muestra en un display color ili92341 y la respuesta se obtiene con un 
EOG implementado con un amplificador diferencial, colocando un electrodo sobre la ceja,
otro debajo del mismo ojo y un tercero como electrodo de referencia en el cuello.

## Conexiones

* Display:
			 |   Display	|   EDU-CIAA	|
			 |:------------:|:-------------:|
			 | 	SDO/MISO 	|	SPI_MISO	|
			 | 	LED		 	| 	3V3	(EXT)	|
			 | 	SCK		 	| 	SPI_SCK		|
			 | 	SDI/MOSI 	| 	SPI_MOSI	|
			 | 	DC/RS	 	| 	GPIO3		|
			 | 	RESET	 	| 	GPIO5		|
			 | 	CS		 	| 	GPIO1		|
			 | 	GND		 	| 	GND			|
			 | 	VCC		 	| 	3V3			|
			 
			 
* EOG:			 
			 |   EOG		|   EDU-CIAA	|
			 |:------------:|:-------------:|
			 | ALIMENTACION |	SPI_MISO	|
			 | GND		 	| 	3V3	(EXT)	|
			 | SALIDA	 	| 	SPI_SCK		|
			
La encuesta comienza con una bienvenida. Luego de unos 6 segundos, comienza la primer pregunta.
El mensaje final es un "Gracias por participar!"
