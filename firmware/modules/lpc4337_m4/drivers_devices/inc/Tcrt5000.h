/*
 * Tcrt5000.h
 *
 *  Created on: 6 sep. 2019
 *      Author: Alumno
 */

#ifndef TCRT5000_H
#define TCRT5000_H

#include "bool.h"
#include "gpio.h"

/* Tcrt5000Init(gpio_t dout):
 * Función que Inicializa el sensor. Recibe el numero
 * de pin y devuelve una validación. Se guarda el pin en variable global mi_pin
 * Se comprueba que el pin esté dentro de los pines habilitados
 */
bool Tcrt5000Init(gpio_t dout);

/*
 * Tcrt5000State(void)
 * Devuelve el estado del puerto (lo que lee el sensor)
 * return TRUE si no encuentra obstaculo, FALSE si encuentra obstaculo
 */
bool Tcrt5000State(void);

bool Tcrt5000Deinit(gpio_t dout); //Desinicializa



#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_TCRT5000_H_ */
