/*
 * Tcrt5000.c
 *
 *  Created on: 6 sep. 2019
 *      Author: Tomy R.
 */
#include "Tcrt5000.h"


gpio_t my_pin;


bool Tcrt5000Init(gpio_t dout)
{
	if (dout>LEDRGB_B)
	{
		GPIOInit(dout, GPIO_INPUT);
		my_pin=dout;

		return (true);
	}
	else
		return (false);

}

bool Tcrt5000State(void)
{
	bool state = GPIORead(my_pin);
	return state;
}


bool Tcrt5000Deinit(gpio_t dout)
{
	GPIODeinit();
}










