/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
typedef struct
{
    char * txt ;          /* Etiqueta de la opción */
    void (*doit)() ;      /* Función a ejecutar en caso de seleccionar esa opción */
} menuEntry ;

void EjecutarMenu (menuEntry *menu, int option){
	menu[option].doit();
}

void PrenderLed (){
	printf("Led Encendido"); /*En el caso de que la función
								necesite argumentos, estos irán dentro
								del doit(argumento)*/
}
void ApagarLed (){
	printf("Led Apagado");
}
void ParpadearLed(){
	printf("Led Pardpadeando");
}



int main(void)
{
	menuEntry menuPrincipal []= {

			{"Prender el Led", PrenderLed },
			{"Apagar el Led", ApagarLed},
			{"Parpadear el Led", ParpadearLed} //EL nombre es puntero
	};

	printf("Ingrese la operación deseada: \n"
			"1: Prender led \n"
			"2: Apagar led \n"
			"3: Hacer parpadear led \n");
	printf("Seleccion:");
	int *i;
	fflush(stdout); //limpio el buffer. Me aparece el scanf primero, si no lo hago.
	scanf("%d", i);

	printf("\nSeleccionado: %d \n", *i);

	EjecutarMenu(menuPrincipal,(*i-1)); //menuPrincipal pasa al puntero menu de la funcion


	return 0;
}

/*==================[end of file]============================================*/

