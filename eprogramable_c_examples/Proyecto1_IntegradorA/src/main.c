/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "stdint.h"

/*==================[macros and definitions]=================================*/
/*typedef enum {
    ON = 0,
    OFF,
    TOGGLE
    } ESTADOS; */

#define ON 1
#define OFF 0
#define TOGGLE 2
/*==================[internal functions declaration]=========================*/
	typedef struct  {
	    uint8_t n_led;      //  indica el número de led a controlar
	    uint8_t n_ciclos;   //indica la cantidad de cilcos de encendido/apagado
	    uint8_t periodo;    //indica el tiempo de cada ciclo
	    uint8_t mode;       //ON, OFF, TOGGLE
	} my_led;

	my_led luz;

void Manejar_led (my_led *a){


	if(a->mode==ON ){
		switch (a->n_led){
		case 1:
			printf("Led 1 ENCENDIDO \n");
			break;
		case 2:
			printf("Led 2 ENCENDIDO \n");
			break;
		case 3:
			printf("Led 3 ENCENDIDO \n");
			break;
		default:
			printf("Error. No existe led seleccionado");

		}
	}
	else if(a->mode==OFF){
			switch (a->n_led){
					case 1:
						printf("Led 1 APAGADO \n");
						break;
					case 2:
						printf("Led 2 APAGADO \n");
						break;
					case 3:
						printf("Led 3 APAGADO \n");
						break;
			}
	}

	else if(a->mode==TOGGLE){
			int i=0;
			int j=0;
			for (i=0;i<a->n_ciclos; i++){
				if(a->n_led==1)
					printf("Led 1 TOGGLEd \n\r");
				else if(a->n_led==2)
					printf("Led 2 TOGGLEd \n\r");
				else if(a->n_led==3)
					printf("Led 3 TOGGLEd \n\r");

				for (j=0;j<a->periodo;j++)
					printf("Retardo \n\r");
			}


		}


	}



int main(void)
{
	my_led a={6,5,10,ON};

	Manejar_led (&a);

	return 0;
}

/*==================[end of file]============================================*/

