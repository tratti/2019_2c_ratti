/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Proyecto1_Ejercicio16/inc/main.h"

#include "stdio.h"
#include <stdint.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/


int main(void)
{

	typedef union {
	    struct {
	        uint8_t var_a;
	        uint8_t var_b;
	        uint8_t var_c;
	        uint8_t var_d;

	    }cada_byte;

	    uint32_t todos;
	} test ;

	test e;

	e.todos= 0x01020304;
	e.cada_byte.var_a=e.todos;
	e.cada_byte.var_b=e.todos>>8;
	e.cada_byte.var_c=e.todos>>16;
	e.cada_byte.var_d=e.todos>>24;

	printf("%d\n", e.cada_byte.var_a);
	printf("%d\n", e.cada_byte.var_b);
	printf("%d\n", e.cada_byte.var_c);
	printf("%d\n", e.cada_byte.var_d);




/* 	Ejercicio a)

 	uint32_t a = 0x01020304 ;

	uint8_t var_a;
	uint8_t var_b;
	uint8_t var_c;
	uint8_t var_d;

	var_a = a;
	var_b= a>>8; //quiero "correr" el ultimo 04 hacia la derecha, lo que corresponde a 8 bits.
	var_c=a>>16; //igual que arriba pero corro 0304 -> 4x4=16bits
	var_d=a>>24;

	printf("%d\n",var_a);
	printf("%d\n",var_b);
	printf("%d\n",var_c);
	printf("%d\n",var_d);
*/


	return 0;
}

/*==================[end of file]============================================*/

